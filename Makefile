COLLECTIONS_PATH ?= ./collections
COLLECTIONS_PATHS ?= $(COLLECTIONS_PATH)/ansible_collections
V ?=
THIS_BASE=$(shell pwd)

.DEFAULT_GOAL := help

CLUSTER_INVENTORY ?= inventory
COMMON_VARS ?= vars.yml

## Ceph args
CEPH_VARS ?= vars.yml
CEPH_TAGS ?= ceph
IFACE =$(shell ip link | grep BROADCAST | grep "state UP" | head -1 | cut -d : -f 2)
ADDR ?= $(shell ip address show dev $(IFACE) | grep "inet " | awk '{print $$2}' | sed 's#/.*##')
CEPH_FSID = 42f7d23e-0e53-11ed-bbf9-a33c93c453ab
KEY_BASE ?= /home/piers/keys

# collections and roles locations
ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections

-include PrivateRules.mak


uninstall:  # uninstall collections
	@rm -rf $(THIS_BASE)/$(COLLECTIONS_PATH)/* \
	   $(THIS_BASE)/$(COLLECTIONS_PATHS)/.collected \
	   $(THIS_BASE)/$(COLLECTIONS_PATH)/.collected

install:  ## Install dependent ansible collections
	@mkdir -p $(THIS_BASE)/$(COLLECTIONS_PATHS)
	@if [ -f $(THIS_BASE)/$(COLLECTIONS_PATHS)/.collected ]; then \
	echo "Allready collected !"; \
	else \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections; \
	ansible-galaxy install -r requirements.yml --roles-path ./playbooks/roles; \
	touch $(THIS_BASE)/$(COLLECTIONS_PATHS)/.collected; \
	fi

reinstall: uninstall install ## reinstall collections

DATA_ROOT=/cephfs
DATA_SIZE=20G
WAL_SIZE=2G

ceph-storage-delete:
	# delete wal disks
	for i in 0 1 2; \
	do \
	sudo umount /dev/ceph_wal$${i}/ceph_wal$${i} 2>/dev/null; \
	sudo lvremove ceph_wal$${i} -y 2>/dev/null; \
	sudo vgremove ceph_wal$${i} 2>/dev/null; \
	sudo pvremove /dev/loop11$${i} 2>/dev/null; \
	sudo losetup -d /dev/loop11$${i} 2>/dev/null; \
	sudo rm -f $(DATA_ROOT)/vdisk_wal$${i}.img; \
	done

	# delete data disks
	for i in 0 1 2; \
	do \
	sudo umount /dev/ceph_data$${i}/ceph_data$${i} 2>/dev/null; \
	sudo lvremove ceph_data$${i} -y 2>/dev/null; \
	sudo vgremove ceph_data$${i} 2>/dev/null; \
	sudo pvremove /dev/loop11$${i} 2>/dev/null; \
	sudo losetup -d /dev/loop12$${i} 2>/dev/null; \
	sudo rm -f $(DATA_ROOT)/vdisk_data$${i}.img; \
	done

	sudo lsblk
	sudo lvdisplay
	sudo vgdisplay
	sudo pvdisplay

ceph-storage: ceph-storage-delete
	sudo mkdir -p $(DATA_ROOT)
	# create wal disks
	for i in 0 1 2; \
	do \
	sudo fallocate -l $(WAL_SIZE) $(DATA_ROOT)/vdisk_wal$${i}.img; \
	sudo losetup -l -P /dev/loop11$${i} $(DATA_ROOT)/vdisk_wal$${i}.img; \
	sudo wipefs -a /dev/loop11$${i}; \
	sudo blkid -i /dev/loop11$${i}; \
	sudo pvcreate /dev/loop11$${i}; \
	sudo vgcreate ceph_wal$${i} /dev/loop11$${i}; \
	sudo lvcreate -l 100%FREE ceph_wal$${i} -n ceph_wal$${i}; \
	done

	# create data disks
	for i in 0 1 2; \
	do \
	sudo fallocate -l $(DATA_SIZE) $(DATA_ROOT)/vdisk_data$${i}.img; \
	sudo losetup -l -P /dev/loop12$${i} $(DATA_ROOT)/vdisk_data$${i}.img; \
	sudo wipefs -a /dev/loop12$${i}; \
	sudo blkid -i /dev/loop12$${i}; \
	sudo pvcreate /dev/loop12$${i}; \
	sudo vgcreate ceph_data$${i} /dev/loop12$${i}; \
	sudo lvcreate -l 100%FREE ceph_data$${i} -n ceph_data$${i}; \
	done

	sudo pvdisplay
	sudo vgdisplay
	sudo lvdisplay
	sudo lsblk

storage:
	# create data disks
	for i in 0 1 2; \
	do \
	sudo lvcreate --size 50G vgubuntu -n ceph_data$${i}; \
	done

	sudo pvdisplay
	sudo vgdisplay
	sudo lvdisplay
	sudo lsblk

.PHONY: ceph

# ceph: ceph-storage ## apply the ceph roles
ceph: ## apply the ceph roles
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CLUSTER_INVENTORY) ./ceph.yml \
					  -e @$(THIS_BASE)/$(COMMON_VARS) \
					  --extra-vars="cephadm_fsid=$(CEPH_FSID)" \
					  --extra-vars="mon_ip=$(ADDR)" \
					  --extra-vars="cephadm_ssh_private_key=$(KEY_BASE)/id_rsa" \
					  --extra-vars="cephadm_ssh_public_key=$(KEY_BASE)/id_rsa.pub" \
					  --tags "$(CEPH_TAGS)" $(V)

.PHONY: commands

# ceph: ceph-storage ## apply the ceph roles
commands: ## apply the ceph roles
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CLUSTER_INVENTORY) ./commands.yml \
					  -e @$(THIS_BASE)/$(COMMON_VARS) \
					  --extra-vars="cephadm_fsid=$(CEPH_FSID)" \
					  --extra-vars="mon_ip=$(ADDR)" \
					  --extra-vars="cephadm_ssh_private_key=$(KEY_BASE)/id_rsa" \
					  --extra-vars="cephadm_ssh_public_key=$(KEY_BASE)/id_rsa.pub" \
					  --tags "$(CEPH_TAGS)" $(V)

.PHONY: cephstatus

cephstatus:  ## ceph status
	sudo /usr/sbin/cephadm shell \
	 --mount /home/piers/code/rpi4/playbooks:/mnt \
	 -c /etc/ceph/ceph.conf \
	 -k /etc/ceph/ceph.client.admin.keyring -- watch ceph -s


.PHONY: ceph-delete

ceph-delete:  ## apply the ceph roles
	[ ! -e $(COMMON_VARS) ] || touch $(COMMON_VARS)
	ANSIBLE_COLLECTIONS_PATH=./collections/ansible_collections:$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(CLUSTER_INVENTORY) ./ceph-delete.yml \
					  -e @$(THIS_BASE)/$(COMMON_VARS) \
					  --extra-vars="$(COMMON_EXTRA_VARS)" \
					  --extra-vars="mon_ip=$(ADDR)" \
					  --extra-vars="cephadm_ssh_private_key=$(KEY_BASE)/id_rsa" \
					  --extra-vars="cephadm_ssh_public_key=$(KEY_BASE)/id_rsa.pub" \
					  --tags "$(CEPH_TAGS)" $(V)
	# make ceph-storage-delete

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
production: false
